package com.ati.ndc.commons.model.orderchange;

import com.ati.ndc.commons.model.XMLNameSpace;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IATA_OrderChangeRQ {
    private XMLNameSpace xmlns;
    private String augmentationPoint;   //value is any element, still wrong datatype
//    private MessageDoc messageDoc;
//    private Party party;  //cant be null
//    private PayloadStandardAttributes payloadAttributes;
//    private POS pos;
//    private Request request;    //cant be null
}