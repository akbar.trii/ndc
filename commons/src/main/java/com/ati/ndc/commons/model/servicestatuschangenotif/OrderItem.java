package com.ati.ndc.commons.model.servicestatuschangenotif;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@XmlRootElement(name = "OrderItem")
@XmlAccessorType(XmlAccessType.FIELD)
public class OrderItem {
    @XmlElement(name = "GrandTotalAmount")
    private Double grandTotalAmount;
    @XmlElement(name = "OrderItemID")
    private String orderItemId; //cant be null
    @XmlElement(name = "OrderItemTypeCode")
    private String orderItemTypeCode;
    @XmlElement(name = "ReusableInd")
    private Boolean reusableInd;
    @XmlElement(name = "Service")
    private List<Service> service;  //cant be null
}