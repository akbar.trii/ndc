package com.ati.ndc.commons.model.servicelist.rq;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "FOPCriteria")
@XmlAccessorType(XmlAccessType.FIELD)
public class FOPCriteria {
    @XmlElement(name = "IATA_EasyPay")
    private IATAEasyPayCriteria iataEasyPay;
    @XmlElement(name = "PaymentCard")
    private PaymentCard paymentCard;
}
