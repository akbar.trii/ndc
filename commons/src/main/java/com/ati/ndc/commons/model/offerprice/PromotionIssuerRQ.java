package com.ati.ndc.commons.model.offerprice;

import com.ati.ndc.commons.model.airshopping.Carrier;
import com.ati.ndc.commons.model.airshopping.Org;
import com.ati.ndc.commons.model.airshopping.TravelAgency;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "PromotionIssuerRQ")
@XmlAccessorType(XmlAccessType.FIELD)
public class PromotionIssuerRQ {
    //choices
    @XmlElement(name = "Carrier")
    private Carrier carrier;    //cant be null
    @XmlElement(name = "Org")
    private Org org;    //cant be null
    @XmlElement(name = "TravelAgency")
    private TravelAgency travelAgency;   //cant be null
}