package com.ati.ndc.commons.model.servicelist.rs;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "SecurePaymentAuthenticationVersion")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class SecurePaymentAuthenticationVersion {
    @XmlElement(name = "CardEnrollmentVersionText")
    private List<String> cardEnrollmentVersionText;
    @XmlElement(name = "SupportedVersionText")
    private List<String> supportedVersionText;  //cant be null
}
