package com.ati.ndc.commons.model.airdocnotif;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "LoyaltyProgramAccount")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoyaltyProgramAccount {
    @XmlElement(name = "AccountNumber")
    private String accountNumber;
    @XmlElement(name = "LoyaltyProgram")
    private LoyaltyProgram loyaltyProgram;  //cant be null
    @XmlElement(name = "ProgramCode")
    private String programCode;
    @XmlElement(name = "ProgramName")
    private String programName;
    @XmlElement(name = "ProviderName")
    private String providerName;
    @XmlElement(name = "SignInID")
    private String signInId;
    @XmlElement(name = "TierCode")
    private String tierCode;
    @XmlElement(name = "TierName")
    private String tierName;
    @XmlElement(name = "TierPriorityText")
    private String tierPriorityText;
    @XmlElement(name = "URL")
    private String url;
}