package com.ati.ndc.commons.model.updateservicenotif;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "Corporation")
@XmlAccessorType(XmlAccessType.FIELD)
public class Corporation {
    @XmlElement(name = "ContactInfoRefID")
    private List<String> contactInfoRefId;
    @XmlElement(name = "CorporateCodeText")
    private String corporateCodeText;
    @XmlElement(name = "CorporateID")
    private String corporateId; //cant be null
    @XmlElement(name = "IATA_Number")
    private Double iataNumber;
    @XmlElement(name = "Name")
    private String name;
}