package com.ati.ndc.commons.model.invreleasenotif;

import com.ati.ndc.commons.model.enumeration.OwnerTypeCode;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "OrderRQ")
@XmlAccessorType(XmlAccessType.FIELD)
public class Order {
    @XmlElement(name = "OrderID")
    private String orderId; //cant be null
    @XmlElement(name = "OrderItem")
    private OrderItem orderItem;
    @XmlElement(name = "OrderVersionNumber")
    private Integer orderVersionNumber;
    @XmlElement(name = "OwnerCode")
    private String ownerCode;   //token with pattern value ([A-Z]{3}|[A-Z]{2})|([0-9][A-Z])|([A-Z][0-9])
    @XmlElement(name = "OwnerTypeCode")
    private OwnerTypeCode ownerTypeCode;
    @XmlElement(name = "WebAddressURI")
    private String webAddressURI;
}