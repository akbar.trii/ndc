package com.ati.ndc.commons.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SecurePaymentAuthenticationInstructionsVersion2 {
    private String acquirerBankIdentificationNumber;   //pattern value [0-9]{1,8}
    private boolean challengeMandateInd;
    private String requestorURI;
}