package com.ati.ndc.commons.model.servicelist.rs;

import com.ati.ndc.commons.model.enumeration.MeasurementSystemCode;
import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@XmlRootElement(name = "ResponseParameters")
@XmlAccessorType(XmlAccessType.FIELD)
public class ResponseParameters {
    @XmlElement(name = "BDC")
    private BDC bdc;
    @XmlElement(name = "CurParameter")
    private List<CurParameter> curParameter;
    @XmlElement(name = "InventoryGuaranteeInd")
    private Boolean inventoryGuaranteeInd;
    @XmlElement(name = "MarketingMessage")
    private List<MarketingMessage> marketingMessage;
    @XmlElement(name = "MeasurementSystemCode")
    private MeasurementSystemCode measurementSystemCode;
    @XmlElement(name = "PaxGroup")
    private PaxGroup paxGroup;
    @XmlElement(name = "ReasonCode")
    private String reasonCode;
}
