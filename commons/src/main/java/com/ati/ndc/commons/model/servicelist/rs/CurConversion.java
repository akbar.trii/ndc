package com.ati.ndc.commons.model.servicelist.rs;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "CurConversion")
@XmlAccessorType(XmlAccessType.FIELD)
public class CurConversion {
    @XmlElement(name = "Amount")
    private Double amount;  //cant be null
    @XmlElement(name = "ConversionRate")
    private Double conversionRate;   //cant be null
    //BaseCurCode
    //BaseMultiplierValue
    //BaseUnitCode
    //CurCode
    //Format
    //MultiplierValue
    //UnitCode
    @XmlElement(name = "LocalAmount")
    private Double localAmount;    //cant be null
}