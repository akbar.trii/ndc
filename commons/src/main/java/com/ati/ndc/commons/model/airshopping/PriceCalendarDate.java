package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@XmlRootElement(name = "PriceCalendarDate")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class PriceCalendarDate {
    @XmlElement(name = "Date")
    private Date date;  //cant be null
    @XmlElement(name = "OriginDestRefID")
    private String originDestRefId;
}