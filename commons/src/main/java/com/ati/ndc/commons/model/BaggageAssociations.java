package com.ati.ndc.commons.model;

import lombok.Data;

import java.util.List;

@Data
public class BaggageAssociations {
    private String baggageAllowanceRefId;   //cant be null
    private BaggageFlightAssociations baggageFlightAssociations;
    private List<String> paxJourneyRefId; //cant be null
    private List<String> paxRefId;    //cant be null
}