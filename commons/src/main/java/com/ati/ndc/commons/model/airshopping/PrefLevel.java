package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "PrefLevel")
@XmlAccessorType(XmlAccessType.FIELD)
public class PrefLevel {
    @XmlElement(name = "PrefContextText")
    private String prefContextText;
    @XmlElement(name = "PrefLevelCode")
    private String prefLevelCode;
}