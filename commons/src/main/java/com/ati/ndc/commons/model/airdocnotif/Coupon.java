package com.ati.ndc.commons.model.airdocnotif;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@XmlRootElement(name = "Coupon")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Coupon {
    @XmlElement(name = "CouponNumber")
    private Double couponNumber;
    @XmlElement(name = "CouponStatusCode")
    private String couponStatusCode;    // token
    @XmlElement(name = "FlightDetails")
    private FlightDetails flightDetails;
    @XmlElement(name = "OrderItem")
    private OrderItem orderItem;
    @XmlElement(name = "RFIC")
    private String rfic;    //with value [0-9A-Z]{1,3}, e.g A (Air Transportation) || C (Baggage) || E (Airport Services) || F (Merchandise) || G (In-flight Services)
    @XmlElement(name = "RFISC")
    private String rfisc;   //e.g 0CC (First Checked Bag) || 0B1 (In-flight Entertainment) || 0BX (Lounge)
    @XmlElement(name = "ServiceDate")
    private Date serviceDate;
    @XmlElement(name = "ServiceName")
    private String serviceName;
    @XmlElement(name = "ServiceQty")
    private Integer serviceQty;
    @XmlElement(name = "ServiceTypeText")
    private String serviceTypeText;
}