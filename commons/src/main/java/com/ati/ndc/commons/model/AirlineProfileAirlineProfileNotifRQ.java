package com.ati.ndc.commons.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "AirlineProfileAirlineProfileNotifRQ")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AirlineProfileAirlineProfileNotifRQ {
    @XmlElement(name = "AirlineProfileDataItem")
    private List<AirlineProfileDataItemAirlineProfileNotifRQ> airlineProfileDataItem;
    @XmlElement(name = "AirlineProfileURL")
    private String airlineProfileURL;
    @XmlElement(name = "AssociatedMediaURL")
    private String associatedMediaURL;
    @XmlElement(name = "AssociationMembershipText")
    private String associationMembershipText;
    @XmlElement(name = "ProfileOwner")
    private CarrierAirlineProfileNotifRQ profileOwner;  //cant be null
//    @XmlElement(name = "SupportedMessageInfo")
//    private List<SupportedMessageInfo> supportedMessageInfo;
}
