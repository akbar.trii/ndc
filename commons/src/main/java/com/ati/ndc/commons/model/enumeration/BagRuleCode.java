package com.ati.ndc.commons.model.enumeration;

public enum  BagRuleCode {
    D,      //Disclosure with NO US DOT Reservation
    N,      //No Disclosure or US DOT Reservation
    Other,   //Other
    Y       //Disclosure and US DOT Reservation</xs:documentation>
}