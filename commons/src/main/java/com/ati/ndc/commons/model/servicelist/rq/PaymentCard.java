package com.ati.ndc.commons.model.servicelist.rq;

import com.ati.ndc.commons.model.airshopping.SecurePayerAuthenticationVersion;
import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "PaymentCard")
@XmlAccessorType(XmlAccessType.FIELD)
public class PaymentCard {
    @XmlElement(name = "CardBrandCode")
    private String cardBrandCode;   //cant be null
    @XmlElement(name = "CardIssuingCountryCode")
    private String cardIssuingCountryCode;  //token with pattern value [A-Z]{2}
    @XmlElement(name = "CardProductTypeCode")
    private String cardProductTypeCode;
    @XmlElement(name = "PaymentRedirectionInd")
    private Boolean paymentRedirectionInd;
    @XmlElement(name = "SecurePaymentAuthenticationVersion")
    private SecurePaymentAuthenticationVersion securePaymentAuthenticationVersion;
}