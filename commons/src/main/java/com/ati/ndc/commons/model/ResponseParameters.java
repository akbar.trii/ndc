package com.ati.ndc.commons.model;

import com.ati.ndc.commons.model.enumeration.MeasurementSystemCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "ResponseParameters")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseParameters {
//    @XmlElement(name = "CurParameter")
//    private List<CurParameter> curParameter;
    @XmlElement(name = "InventoryGuaranteeInd")
    private Boolean inventoryGuaranteeInd;
//    @XmlElement(name = "LangUsage")
//    private List<LangUsage> langUsage;
    @XmlElement(name = "MeasurementSystemCode")
    private MeasurementSystemCode measurementSystemCode;
//    @XmlElement(name = "PricingParameter")
//    private PricingParameter pricingParameter;
}
