package com.ati.ndc.commons.model.servicelist.rs;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@XmlRootElement(name = "ServiceListProcessing")
@XmlAccessorType(XmlAccessType.FIELD)
public class ServiceListProcessing {
    @XmlElement(name = "MarketingMessage")
    private List<Remark> marketingMessage;
}
