package com.ati.ndc.commons.model.enumeration;

public enum TaxTypeCode {
    Applied,
    Exempt
}
