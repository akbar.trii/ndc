package com.ati.ndc.commons.model;

import com.ati.ndc.commons.model.enumeration.MatchTypeCode;
import com.ati.ndc.commons.model.enumeration.OwnerTypeCode;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class Offer {
    private List<BaggageAssociations> baggageAllowance;
    private List<String> baggageDisclosureRefId;
//    private Commission commission;
//    private List<Desc> desc;
    private String disclosureRefId;
//    private JourneyOverview journeyOverview;
    private String matchAppText;
    private Double matchPercent;
    private MatchTypeCode matchTypeCode;
    private Date offerExpirationTimeLimitDateTime;
    private String offerId; //cant be null
    private List<OfferItem> offerItem;  //cant be null
    private String OwnerCode;   //token with pattern value ([A-Z]{3}|[A-Z]{2})|([0-9][A-Z])|([A-Z][0-9]) and cant be null
    private OwnerTypeCode ownerTypeCode;
    private List<String> penaltyRefId;
//    private List<PTCOfferParameters> ptcOfferParameters;
    private Boolean redemptionInd;
    private Boolean requestedDateInd;
    private Double totalPrice;
    private String validatingCarrierCode;   //token with pattern value ([A-Z]{3}|[A-Z]{2})|([0-9][A-Z])|([A-Z][0-9])
    private String webAddressURL;
}