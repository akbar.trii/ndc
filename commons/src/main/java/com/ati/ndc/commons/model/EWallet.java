package com.ati.ndc.commons.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EWallet {
    private String authorizationId; //max length 35 and cant be null
    private Double netClearanceAmount;  //cant be null
}