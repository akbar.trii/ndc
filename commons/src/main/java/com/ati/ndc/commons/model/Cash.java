package com.ati.ndc.commons.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Cash {
//    private PostalAddress collectionAddress;
    private String receiptId;   //token
    private SettlementData settlementData;
    private String terminalId;  //token
//    private TravelAgent travelAgent;
}