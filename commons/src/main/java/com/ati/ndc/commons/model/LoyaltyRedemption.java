package com.ati.ndc.commons.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoyaltyRedemption {
    private Integer certificateNumber;
    private Double loyaltyCurAmount;
//    private LoyaltyProgramAccount loyaltyProgramAccount;
}
