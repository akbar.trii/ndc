package com.ati.ndc.commons.model.invreleasenotif;

import com.ati.ndc.commons.model.enumeration.OwnerTypeCode;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "OrderItem")
@XmlAccessorType(XmlAccessType.FIELD)
public class OrderItem {
    @XmlElement(name = "GrandTotalAmount")
    private Double grandTotalAmount;
    @XmlElement(name = "OrderItemID")
    private String orderItemId; //cant be null
    @XmlElement(name = "OrderItemTypeCode")
    private String orderItemTypeCode;
    @XmlElement(name = "OwnerCode")
    private String ownerCode;   //token with pattern value ([A-Z]{3}|[A-Z]{2})|([0-9][A-Z])|([A-Z][0-9])
    @XmlElement(name = "OwnerTypeCode")
    private OwnerTypeCode ownerTypeCode;
    @XmlElement(name = "ReusableInd")
    private Boolean reusableInd;
    @XmlElement(name = "Service")
    private Service service;
    @XmlElement(name = "WebAddressURI")
    private String webAddressURI;
}