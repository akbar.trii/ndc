package com.ati.ndc.commons.model.updateservicenotif;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "BagDimension")
@XmlAccessorType(XmlAccessType.FIELD)
public class BagDimension {
    @XmlElement(name = "HeightMeasure")
    private Double heightMeasure;
    @XmlElement(name = "LengthMeasure")
    private Double lengthMeasure;
    @XmlElement(name = "WidthMeasure")
    private Double widthMeasure;
}
