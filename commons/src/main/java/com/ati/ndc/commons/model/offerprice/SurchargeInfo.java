package com.ati.ndc.commons.model.offerprice;

import com.ati.ndc.commons.model.enumeration.RoundingPrecisionCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "SurchargeInfo")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SurchargeInfo {
    @XmlElement(name = "AmountRangeMaximumAmount")
    private Double amountRangeMaximumAmount;
    @XmlElement(name = "AmountRangeMinimumAmount")
    private Double amountRangeMinimumAmount;
    @XmlElement(name = "PercentageRangeMaximumPercent")
    private Double percentageRangeMaximumPercent;
    @XmlElement(name = "PercentageRangeMinimumPercent")
    private Double percentageRangeMinimumPercent;
    @XmlElement(name = "RoundingNumberOfDecimalsNumber")
    private Double roundingNumberOfDecimalsNumber;
    @XmlElement(name = "RoundingPrecisionCode")
    private RoundingPrecisionCode roundingPrecisionCode;
}
