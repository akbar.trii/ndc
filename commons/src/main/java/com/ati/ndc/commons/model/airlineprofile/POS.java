package com.ati.ndc.commons.model.airlineprofile;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "POS")
@XmlAccessorType(XmlAccessType.FIELD)
public class POS {
    @XmlElement(name = "AgentDutyText")
    private String agentDutyText;
    @XmlElement(name = "City")
    private City city;
    @XmlElement(name = "Country")
    private Country country;
    @XmlElement(name = "RequestTime")
    private Date requestTime;
}