package com.ati.ndc.commons.model.ordercancel.rs;

import com.ati.ndc.commons.model.airlineprofile.IATAAirlineProfileRS;
import lombok.Data;

@Data
public class IATA_OrderCancelRS extends IATAAirlineProfileRS {
}