package com.ati.ndc.commons.model.offerprice;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "SelectedBundleServices")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SelectedBundleServices {
    @XmlElement(name = "SelectedServiceDefinitionRefID")
    private List<String> selectedServiceDefinitionRefId;
    @XmlElement(name = "SelectedServiceRefID")
    private String selectedServiceRefId;    //cant be null
}
