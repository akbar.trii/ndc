package com.ati.ndc.commons.model.servicedelivery.rq;

import com.ati.ndc.commons.model.orderclosingnotif.IATA_OrderClosingNotifRQ;
import lombok.Data;

@Data
public class IATA_ServiceDeliveryRQ extends IATA_OrderClosingNotifRQ {

}