package com.ati.ndc.commons.model.enumeration;

public enum OwnerTypeCode {
    ORA, //Offer Responsible Airline
    POA //Participating Offer Airline
}
