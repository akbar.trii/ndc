package com.ati.ndc.commons.model.enumeration;

public enum LevelTypeCode {
    Full,   //Fully Refundable
    None,   //Non Refundable
    Partial
}
