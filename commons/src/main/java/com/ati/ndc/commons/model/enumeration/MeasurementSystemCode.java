package com.ati.ndc.commons.model.enumeration;

public enum MeasurementSystemCode {
    Imperial,
    Metric
}
