package com.ati.ndc.commons.model.offerprice;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "FareGroupRQ")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class FareGroupRQ {
    @XmlElement(name = "FareCode")
    private String fareCode;    //cant be null and e.g 70J - Published Fares, 70K - Dynamic Discounted Fares, 749 - Negotiated Fares, 756 - One Way Fare, 758 - Private Fares
    @XmlElement(name = "FareDetail")
    private List<FareDetailRQ> fareDetail;
    @XmlElement(name = "FareGroupID")
    private String fareGroupId; //cant be null
}
