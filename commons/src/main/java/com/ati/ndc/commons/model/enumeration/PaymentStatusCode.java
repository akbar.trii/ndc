package com.ati.ndc.commons.model.enumeration;

public enum PaymentStatusCode {
    ACCEPTED,   //The commitments to pay was accepted./At the Payment Level to indicate that a payment has been authorized, etc. or the status in not needed
    ALLOCATED,  //The value of compensation was allocated to the corresponding Order Item.
    CLOSED,     //The payment process was done.
    COMMITTED,  //The payer has committed to pay the compensation./When NDC indicates that part of a payment is to be applied to an order item
    RECEIVED,   //The compensation was received./ The actual receipt of funds is a function of the accounting system, or it it expected that the revenue accounting system will update the order when the bank transmits funds to the airline? Don't think we need this.
    REFUNDED,   //The compensation was transfered back to the payer./When an order item is cancelled or changed, we expect that any payments applied to the item will be un-applied. this leaves a credit balance on the order that can be applied to a new item of refunded. 
    SENT,       //The compensation was transfered.
}
