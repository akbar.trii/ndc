package com.ati.ndc.commons.model.orderchangenotif;

import com.ati.ndc.commons.model.Notif;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IATA_OrderChangeNotifRQ {
    private String augmentationPoint;   //any element // still wrong datatype
//    private MessageDoc messageDoc;
//    private Notif notif;//cant be null
//    private Party party;    //cant be null
//    private PayloadStandardAttributes payloadAttributes;
}