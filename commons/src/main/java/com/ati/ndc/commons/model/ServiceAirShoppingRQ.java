package com.ati.ndc.commons.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@XmlRootElement(name = "ServiceAirShoppingRQ")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ServiceAirShoppingRQ {
//    @XmlElement(name = "BookingRef")
//    private BookingRef bookingRef;  //max 3 elements
    @XmlElement(name = "InventoryGuaranteeDateTime")
    private Date inventoryGuaranteeDateTime;
    @XmlElement(name = "ServiceID")
    private String serviceId;
    @XmlElement(name = "UnchangedInd")
    private Boolean unchangedInd;
//    @XmlElement(name = "ValidatingCarrier")
//    private Carrier ValidatingCarrier;
}
