package com.ati.ndc.commons.model.paymentclearancelist.rq;

import com.ati.ndc.commons.model.ClearanceFilterCriteria;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IATA_PaymentClearanceListRQ {
    // one of these element shud has value (choice)
    private List<String> airlineRefId;    //token minLength 1 maxLength 15 with pattern value ([A-Za-z0-9]{1,15}) and cant be null
    private List<ClearanceFilterCriteria> clearanceFilterCriteria;  //cant be null
    private List<String> clearanceId;   //token length 15 with pattern ([0-9]{7}[A-Za-z0-9]{8}) and cant be null
    private List<String> otherId;   //shud check again
    //end

//    private PayloadStandardAttributes payloadStandardAttributes;


}