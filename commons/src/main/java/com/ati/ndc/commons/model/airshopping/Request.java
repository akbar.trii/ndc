package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "Request")
@XmlAccessorType(XmlAccessType.FIELD)
public class Request {
    @XmlElement(name = "FlightRequest")
    private FlightRequestType flightRequest;    //cant be null
    @XmlElement(name = "Paxs")
    private Paxs paxs;
    @XmlElement(name = "ResponseParameters")
    private ResponseParameters responseParameters;
    @XmlElement(name = "ShoppingCriteria")
    private ShoppingCriteria shoppingCriteria;
}
