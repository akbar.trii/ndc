package com.ati.ndc.commons.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "PartyAirDocNotifRQ")
@XmlAccessorType(XmlAccessType.FIELD)
public class PartyAirDocNotifRQ {
    @XmlElement(name = "Participant")
    private List<ParticipantAirDocNotifRQ> participant;
    @XmlElement(name = "Recipient")
    private PartyTypeAirDocNotifRQ recipient;
    @XmlElement(name = "Sender")
    private PartyTypeAirDocNotifRQ sender; //cant be null
}