package com.ati.ndc.commons.model.paymentclearance.rq;

import com.ati.ndc.commons.model.paymentclearancenotif.IATA_PaymentClearanceNotif;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IATA_PaymentClearanceRQ extends IATA_PaymentClearanceNotif {
    private Boolean rejectEveryClearanceIndicator;
}