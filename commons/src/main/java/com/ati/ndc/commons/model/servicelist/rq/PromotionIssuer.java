package com.ati.ndc.commons.model.servicelist.rq;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "PromotionIssuer")
@XmlAccessorType(XmlAccessType.FIELD)
public class PromotionIssuer {
    //choices
    @XmlElement(name = "Carrier")
    private Carrier carrier;    //cant be null
    @XmlElement(name = "Org")
    private Org org;    //cant be null
    @XmlElement(name = "TravelAgency")
    private TravelAgency travelAgency;   //cant be null
}