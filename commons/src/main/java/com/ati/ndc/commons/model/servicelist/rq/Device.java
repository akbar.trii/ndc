package com.ati.ndc.commons.model.servicelist.rq;

import com.ati.ndc.commons.model.enumeration.DeviceOwnerTypeCode;
import com.ati.ndc.commons.model.enumeration.PresenceTypeCode;
import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "Device")
@XmlAccessorType(XmlAccessType.FIELD)
public class Device {
    @XmlElement(name = "DeviceCode")
    private String deviceCode;
    @XmlElement(name = "DeviceOwnerTypeCode")
    private DeviceOwnerTypeCode deviceOwnerTypeCode;
    @XmlElement(name = "Phone")
    private Phone phone;
    @XmlElement(name = "PresenceTypeCode")
    private PresenceTypeCode presenceTypeCode;
}