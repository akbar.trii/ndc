package com.ati.ndc.commons.model.offerprice;

import com.ati.ndc.commons.model.enumeration.BaggageTypeCodeType;
import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "BaggageAllowance")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class BaggageAllowance {
    @XmlElement(name = "ApplicableBagText")
    private String applicableBagText;
    @XmlElement(name = "ApplicablePartyText")
    private String applicablePartyText;
    @XmlElement(name = "BaggageAllowanceID")
    private String baggageAllowanceId; //cant be null
    @XmlElement(name = "BDC")
    private BDC bdc;
    @XmlElement(name = "DescText")
    private List<String> descText;  //max 99 elements
    @XmlElement(name = "DimensionAllowance")
    private List<PieceDimensionAllowance> dimensionAllowance;
    @XmlElement(name = "PieceAllowance")
    private List<PieceAllowance> pieceAllowance;
    @XmlElement(name = "RFISC")
    private String rfisc;
    @XmlElement(name = "TypeCode")
    private BaggageTypeCodeType typeCode;   //cant be null
    @XmlElement(name = "WeightAllowance")
    private List<WeightAllowance> weightAllowance;
}