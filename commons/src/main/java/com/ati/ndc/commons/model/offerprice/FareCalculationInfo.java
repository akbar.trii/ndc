package com.ati.ndc.commons.model.offerprice;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "FareCalculationInfo")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class FareCalculationInfo {
    @XmlElement(name = "AddlInfoText")
    private String addlInfoText;    //cant be null
    @XmlElement(name = "PricingCodeText")
    private String pricingCodeText; //cant be null
    @XmlElement(name = "ReportingCodeText")
    private String reportingCodeText;   //cant be null
}
