package com.ati.ndc.commons.model.updateservicenotif;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "BagTag")
@XmlAccessorType(XmlAccessType.FIELD)
public class BagTag {
    @XmlElement(name = "BagTagID")
    private String bagTagId;
    @XmlElement(name = "IssuingCarrier")
    private Carrier issuingCarrier;
    @XmlElement(name = "TagExpediteInd")
    private Boolean tagExpediteInd;
    @XmlElement(name = "TagFallbackInd")
    private Boolean tagFallbackInd;
    @XmlElement(name = "TagInterlineInd")
    private Boolean tagInterlineInd;
}
