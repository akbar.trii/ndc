package com.ati.ndc.commons.model.offerprice;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "FareRule")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class FareRule {
    @XmlElement(name = "InstantPurchaseTypeCode")
    private String instantPurchaseTypeCode;
    @XmlElement(name = "PenaltyRefID")
    private List<String> penaltyRefId;
    @XmlElement(name = "Remark")
    private List<Remark> remark;
    @XmlElement(name = "RuleCode")
    private String ruleCode;
    @XmlElement(name = "TicketlessInd")
    private Boolean ticketlessInd;
}
