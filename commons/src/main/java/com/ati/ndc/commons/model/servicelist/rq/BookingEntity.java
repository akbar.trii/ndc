package com.ati.ndc.commons.model.servicelist.rq;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "BookingEntity")
@XmlAccessorType(XmlAccessType.FIELD)
public class BookingEntity {
    //choices
    @XmlElement(name = "Carrier")
    private Carrier carrier;
    @XmlElement(name = "Org")
    private Org Org;
}