package com.ati.ndc.commons.model.updateservicenotif;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "DeliveryProvider")
@XmlAccessorType(XmlAccessType.FIELD)
public class DeliveryProvider {
    @XmlElement(name = "ID")
    private String id;  //cant be null
    @XmlElement(name = "Name")
    private String Name;    //cant be null
}
