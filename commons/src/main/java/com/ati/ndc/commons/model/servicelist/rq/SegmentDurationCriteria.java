package com.ati.ndc.commons.model.servicelist.rq;

import com.ati.ndc.commons.model.enumeration.PrefLevelCodeType;
import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "SegmentDurationCriteria")
@XmlAccessorType(XmlAccessType.FIELD)
public class SegmentDurationCriteria {
    @XmlElement(name = "MaximumTimeMeasure")
    private Double maximumTimeMeasure;
    @XmlElement(name = "PrefCode")
    private PrefLevelCodeType prefCode;
}
