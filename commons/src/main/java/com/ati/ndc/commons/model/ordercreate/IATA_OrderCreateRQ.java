package com.ati.ndc.commons.model.ordercreate;

import com.ati.ndc.commons.model.orderchange.IATA_OrderChangeRQ;
import lombok.Data;

@Data
public class IATA_OrderCreateRQ extends IATA_OrderChangeRQ {
}