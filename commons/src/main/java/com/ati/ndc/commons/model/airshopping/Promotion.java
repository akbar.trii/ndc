package com.ati.ndc.commons.model.airshopping;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "Promotion")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Promotion {
    @XmlElement(name = "OwnerName")
    private String ownerName;
    @XmlElement(name = "PaxRefID")
    private String paxRefId;
    @XmlElement(name = "PromotionID")
    private String promotionId;
    @XmlElement(name = "PromotionIssuer")
    private PromotionIssuer promotionIssuer;    //sequential not choices
    @XmlElement(name = "Remark")
    private List<Remark> remark;
    @XmlElement(name = "URL")
    private String url;
}