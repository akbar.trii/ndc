package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "RedressCaseRQ")
@XmlAccessorType(XmlAccessType.FIELD)
public class RedressCaseRQ {
    @XmlElement(name = "ProgramName")
    private String programName;
    @XmlElement(name = "RedressCaseID")
    private String redressCaseId; //cant be null
}