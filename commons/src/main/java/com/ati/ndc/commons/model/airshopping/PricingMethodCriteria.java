package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "PricingMethodCriteria")
@XmlAccessorType(XmlAccessType.FIELD)
public class PricingMethodCriteria {
//    Examples:
//    Y (BEST FARE IN SAME CABIN CLASS)
//    C (BEST FARE ACROSS ALL CABIN CLASSES)
//    N (PRICE ACCORDINGLY TO OTHER DESIGNATED PRICING PARAMETERS)
    @XmlElement(name = "BestPricingOptionText")
    private String bestPricingOptionText;   //cant be null
}
