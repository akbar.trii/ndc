package com.ati.ndc.commons.model;

import com.ati.ndc.commons.model.enumeration.OwnerTypeCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "OrderInvGuaranteeRQ")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderInvGuaranteeRQ {
    @XmlElement(name = "OrderID")
    private String orderId;
    @XmlElement(name = "OrderItem")
    private OrderItemInvGuaranteeRQ orderItem;
    @XmlElement(name = "OrderVersionNumber")
    private Integer orderVersionNumber;
    @XmlElement(name = "OwnerCode")
    private String ownerCode; //token with pattern value ([A-Z]{3}|[A-Z]{2})|([0-9][A-Z])|([A-Z][0-9]) and cant be null
    @XmlElement(name = "OwnerTypeCode")
    private OwnerTypeCode ownerTypeCode;
    @XmlElement(name = "WebAddressURI")
    private String webAddressURI; //URI
}
