package com.ati.ndc.commons.model.invguarantee;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "ShoppingResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class ShoppingResponseRS {
    @XmlElement(name = "OwnerCode")
    private String ownerCode;   //token with pattern value ([A-Z]{3}|[A-Z]{2})|([0-9][A-Z])|([A-Z][0-9])
    @XmlElement(name = "ShoppingResponseRefID")
    private String shoppingResponseRefId;;
}
