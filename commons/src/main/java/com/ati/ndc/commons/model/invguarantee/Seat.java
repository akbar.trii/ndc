package com.ati.ndc.commons.model.invguarantee;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "Seat")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class Seat {
    @XmlElement(name = "ColumnID")
    private String columnId;
    @XmlElement(name = "RowNumber")
    private Integer rowNumber;
    @XmlElement(name = "SeatCharacteristicCode")
    private List<String> seatCharacteristicCode;    //max 99 elements
}
