package com.ati.ndc.commons.model.updateservicenotif;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "Request")
@XmlAccessorType(XmlAccessType.FIELD)
public class Request {
    @XmlElement(name = "DataLists")
    private List<DataLists> dataLists;  //cant be null
    @XmlElement(name = "Order")
    private List<Order> order;  //cant be null
}
