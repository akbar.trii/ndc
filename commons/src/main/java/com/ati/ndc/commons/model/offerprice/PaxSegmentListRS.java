package com.ati.ndc.commons.model.offerprice;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "PaxSegmentListRS")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class PaxSegmentListRS {
        @XmlElement(name = "PaxSegment")
        private List<PaxSegmentRS> paxSegment;   //cant be null
}
