package com.ati.ndc.commons.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BankTransfer {
    private String accountTypeText;
    private String bankAccountI;    //token
    private String bankId;  //token
    private Integer checkNumber;
    private String ownerName;
    private String subCode; //token
}