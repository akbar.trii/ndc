package com.ati.ndc.commons.model.paymentclearancelist;

import com.ati.ndc.commons.model.orderchange.IATA_OrderChangeRQ;
import lombok.Data;

@Data
public class IATA_AirShoppingRQ extends IATA_OrderChangeRQ {
}