package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;
import java.util.List;

@XmlRootElement(name = "OfferItem")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class OfferItem {
    @XmlElement(name = "CancelRestrictions")
    private List<CancelRestrictions> cancelRestrictions;
    @XmlElement(name = "ChangeRestrictions")
    private List<ChangeRestrictions> changeRestrictions;
    @XmlElement(name = "Commission")
    private List<Commission> commission;
    @XmlElement(name = "Desc")
    private List<DescType> desc;
    @XmlElement(name = "FareDetail")
    private List<FareDetail> fareDetail;
    @XmlElement(name = "MandatoryInd")
    private Boolean mandatoryInd;
    @XmlElement(name = "ModificationProhibitedInd")
    private Boolean modificationProhibitedInd;
    @XmlElement(name = "OfferItemID")
    private String offerItemId; //cant be null
    @XmlElement(name = "OfferItemPaymentTimeLimit")
    private AlaCarteOfferItemPaymentTimeLimit offerItemPaymentTimeLimit;    //cant be null
    @XmlElement(name = "OfferItemTypeCode")
    private String offerItemTypeCode;
    @XmlElement(name = "Price")
    private Price price;    //cant be null
    @XmlElement(name = "PriceGuaranteeTimeLimitDateTime")
    private Date priceGuaranteeTimeLimitDateTime;
    @XmlElement(name = "Service")
    private List<Service> service;   //cant be null
    @XmlElement(name = "ServiceTaxonomy")
    private List<ServiceTaxonomy> serviceTaxonomy;
    @XmlElement(name = "StopOverRestrictions")
    private StopOverRestrictions stopOverRestrictions;
}
