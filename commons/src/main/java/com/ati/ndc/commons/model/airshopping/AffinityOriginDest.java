package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@XmlRootElement(name = "AffinityOriginDest")
@XmlAccessorType(XmlAccessType.FIELD)
public class AffinityOriginDest {
    @XmlElement(name = "AffinityArrivalRequest")
    private AffinityArrivalRequest affinityArrivalRequest;
    @XmlElement(name = "AffinityDepRequest")
    private AffinityArrivalRequest affinityDepRequest;
    @XmlElement(name = "ConnectionPrefRefID")
    private String connectionPrefRefId;
    @XmlElement(name = "PreferredCabinType")
    private List<CabinType> preferredCabinType;
}
