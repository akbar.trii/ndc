package com.ati.ndc.commons.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Clearance {
    private String clearanceId; //token length value 15 with pattern value ([0-9]{7}[A-Za-z0-9]{8}) and cant be null
    private String failureReasonCode;   //token length value 7 with pattern value [A-Za-z0-9]{7}
    private String statusCode;  //token length value 5 with pattern value [A-Za-z0-9]{5} and cant be null
}
