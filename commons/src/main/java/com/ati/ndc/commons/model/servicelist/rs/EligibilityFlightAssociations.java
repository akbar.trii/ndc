package com.ati.ndc.commons.model.servicelist.rs;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "EligibilityFlightAssociations")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class EligibilityFlightAssociations {
    //choices
    @XmlElement(name = "DatedOperatingLegRef")
    private List<DatedOperatingLegRef> datedOperatingLegRef;
    @XmlElement(name = "PaxJourneyRef")
    private List<PaxJourneyRef> paxJourneyRef;
    @XmlElement(name = "PaxSegmentRef")
    private List<PaxSegmentRef> paxSegmentRef;
}
