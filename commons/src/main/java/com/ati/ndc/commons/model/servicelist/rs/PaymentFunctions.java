package com.ati.ndc.commons.model.servicelist.rs;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "PaymentFunctions")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class PaymentFunctions {
    @XmlElement(name = "PaymentSupportedMethod")
    private List<PaymentSupportedMethod> paymentSupportedMethod;
}
