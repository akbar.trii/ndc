package com.ati.ndc.commons.model.enumeration;

public enum GenderCode {
    F,  //Female
    M,  //Male
    X   //Unspecified
}
