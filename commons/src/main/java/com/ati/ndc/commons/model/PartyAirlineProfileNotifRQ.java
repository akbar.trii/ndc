package com.ati.ndc.commons.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "PartyAirlineProfileNotifRQ")
@XmlAccessorType(XmlAccessType.FIELD)
public class PartyAirlineProfileNotifRQ {
    @XmlElement(name = "Participant")
    private List<ParticipantAirlineProfileNotifRQ> participant;
    @XmlElement(name = "Recipient")
    private PartyTypeAirlineProfileNotifRQ recipient;
    @XmlElement(name = "Sender")
    private PartyTypeAirlineProfileNotifRQ sender; //cant be null
}
