package com.ati.ndc.commons.model.enumeration;

public enum ConsequenceOfInactionCodeType {
    Auto_acceptance,    //Auto-acceptance by airline    (Auto-acceptance)
	Auto_cancellation   //Auto-cancellation by airline  (Auto-cancellation)
}
