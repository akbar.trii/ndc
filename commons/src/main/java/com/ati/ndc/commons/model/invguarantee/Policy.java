package com.ati.ndc.commons.model.invguarantee;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "Policy")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class Policy {
    @XmlElement(name = "Desc")
    private List<Desc> desc;
    @XmlElement(name = "OwnerName")
    private String ownerName;
    @XmlElement(name = "PolicyNodeInfo")
    private List<PolicyNodeInfo> policyNodeInfo;    //cant be null
    @XmlElement(name = "PolicyTypeText")
    private String policyTypeText;
    @XmlElement(name = "VersionNumber")
    private Double versionNumber;
}