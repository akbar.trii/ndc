package com.ati.ndc.commons.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PaymentSupportedMethod {
//    private List<OrderAssociation> orderAssociation;
    private OtherPaymentMethod otherPaymentMethod;
    private PaymentCard paymentCard;
    private PaymentRedirection paymentRedirection;
//    private SurchargeInfo surchargeInfo;
    private String TypeCode;    //token and cant be null
}