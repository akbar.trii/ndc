package com.ati.ndc.commons.model.airdocnotif;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@XmlRootElement(name = "DocNotification")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DocNotification {
    @XmlElement(name = "Coupon")
    private Coupon coupon; // cant be null and max 4
    @XmlElement(name = "IssueDate")
    private Date issueDate;
    @XmlElement(name = "Order")
    private Order order; //cant be null
    @XmlElement(name = "Pax")
    private Pax pax; // cant be null
    @XmlElement(name = "TicketDocTypeCode")
    private String ticketDocTypeCode;
    @XmlElement(name = "TicketNumber")
    private String ticketNumber;
}
