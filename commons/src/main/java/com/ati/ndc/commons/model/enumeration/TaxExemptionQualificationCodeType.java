package com.ati.ndc.commons.model.enumeration;

public enum TaxExemptionQualificationCodeType {
    PSU,    //The commitments to pay was accepted./At the Payment Level to indicate that a payment has been authorized, etc. or the status in not needed
    SEZ,    //The payment process was done.
    UN      //The value of compensation was allocated to the corresponding Order Item.
}