package com.ati.ndc.commons.model.enumeration;

public enum ChangeTypeCode {
    Flight, //This identifies Flight detail changes. changes to the core flight details. departure and arrival points and airline.
    Name,   //Name based changes. Changes to the passenger(s) names.
    Service //This identifies Service non flight changes. seat preferences/food/baggage etc.
}
