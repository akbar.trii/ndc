package com.ati.ndc.commons.model.airlineprofile;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "AirlineProfileFilterCriteria")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AirlineProfileFilterCriteria {
    @XmlElement(name = "AirlineProfile")
    private List<AirlineProfileType> airlineProfile;
    @XmlElement(name = "AllProfilesInd")
    private Boolean allProfilesInd;
    @XmlElement(name = "MediaURL_Ind")
    private Boolean mediaURLInd;
}