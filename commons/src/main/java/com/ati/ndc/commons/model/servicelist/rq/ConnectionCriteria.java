package com.ati.ndc.commons.model.servicelist.rq;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;
import java.util.List;

@Data
@XmlRootElement(name = "ConnectionCriteria")
@XmlAccessorType(XmlAccessType.FIELD)
public class ConnectionCriteria {
    @XmlElement(name = "ConnectionPrefID")
    private String ConnectionPrefId;    //cant be null
    @XmlElement(name = "ConnectionPricingInd")
    private Boolean connectionPricingInd;
    @XmlElement(name = "InterlineInd")
    private Boolean interlineInd;
    @XmlElement(name = "MaximumConnectionQty")
    private Double maximumConnectionQty;
    @XmlElement(name = "MaximumConnectionTime")
    private Date maximumConnectionTime;
    @XmlElement(name = "MinimumConnectionTime")
    private Date minimumConnectionTime;
    @XmlElement(name = "StationCriteria")
    private List<StationCriteria> stationCriteria;  //cant be null
}
