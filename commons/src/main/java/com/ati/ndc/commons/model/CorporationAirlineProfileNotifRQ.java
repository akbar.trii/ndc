package com.ati.ndc.commons.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "CorporationAirlineProfileNotifRQ")
@XmlAccessorType(XmlAccessType.FIELD)
public class CorporationAirlineProfileNotifRQ {
    @XmlElement(name = "ContactInfo")
    private List<ContactInfoAirlineProfileNotifRQ> contactInfo;
    @XmlElement(name = "CorporateCodeText")
    private String corporateCodeText;
    @XmlElement(name = "CorporateID")
    private String corporateId; //cant be null
    @XmlElement(name = "IATA_Number")
    private Double iataNumber;
    @XmlElement(name = "Name")
    private String name;
}