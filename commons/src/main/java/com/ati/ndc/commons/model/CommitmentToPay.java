package com.ati.ndc.commons.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommitmentToPay {
    private Agreement agreement;    //cant be null
    private String commitmentId;    //token max length 35 and cant be null
    private Date paymentCommitmentDateTime; //cant be null
    private PaymentMethodType paymentMethod;    //cant be null
    private String typeCode;    //token and cant be null
}
