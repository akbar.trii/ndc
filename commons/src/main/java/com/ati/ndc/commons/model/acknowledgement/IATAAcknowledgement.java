package com.ati.ndc.commons.model.acknowledgement;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "IATA_Acknowledgement")
@XmlAccessorType(XmlAccessType.FIELD)
public class IATAAcknowledgement {
    @XmlElement(name = "MessageDoc")
    private MessageDoc messageDoc;
    @XmlElement(name = "Notification")
    private Notification notification;
    @XmlElement(name = "PayloadAttributes")
    private PayloadStandardAttributes payloadAttributes;

    public IATAAcknowledgement() {}

    public IATAAcknowledgement(MessageDoc messageDoc, Notification notification, PayloadStandardAttributes payloadAttributes) {
        this.messageDoc = messageDoc;
        this.notification = notification;
        this.payloadAttributes = payloadAttributes;
    }

    public MessageDoc getMessageDoc() {
        return messageDoc;
    }

    public void setMessageDoc(MessageDoc messageDoc) {
        this.messageDoc = messageDoc;
    }

    public Notification getNotification() {
        return notification;
    }

    public void setNotification(Notification notification) {
        this.notification = notification;
    }

    public PayloadStandardAttributes getPayloadAttributes() {
        return payloadAttributes;
    }

    public void setPayloadAttributes(PayloadStandardAttributes payloadAttributes) {
        this.payloadAttributes = payloadAttributes;
    }
}