package com.ati.ndc.commons.model;

import com.ati.ndc.commons.model.enumeration.DeviceOwnerTypeCode;
import com.ati.ndc.commons.model.enumeration.PresenceTypeCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Device {
    private String browserAcceptHeaderText;
    private String browserUserAgentHeaderText;
    private String deviceCode;  //token and cant be null
    private String deviceName;  //token
    private DeviceOwnerTypeCode deviceOwnerTypeCode;
    private String ipAddressText;
    private String macAddress;
//    private Phone phone;
    private PresenceTypeCode presenceTypeCode;  //cant be null
    private String touchPointTrxCode;   //token
}