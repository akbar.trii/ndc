package com.ati.ndc.commons.model.invguarantee;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "BookingEntity")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class BookingEntity {
    //choices but can be null
    @XmlElement(name = "Carrier")
    private Carrier carrier;
    @XmlElement(name = "Org")
    private Org org;

    public Carrier getCarrier() {
        return carrier;
    }

    public void setCarrier(Carrier carrier) {
        this.carrier = carrier;
    }

    public Org getOrg() {
        return org;
    }

    public void setOrg(Org org) {
        this.org = org;
    }
}
