package com.ati.ndc.commons.model.enumeration;

public enum TripPurposeCodeType {
    Business,
    BusinessAndLeisure,
    Leisure,
    Other,
    Unknown
}
