package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@Data
@XmlRootElement(name = "AffinityArrivalRequest")
@XmlAccessorType(XmlAccessType.FIELD)
public class AffinityArrivalRequest {
    @XmlElement(name = "Country")
    private Country country;
    @XmlElement(name = "CountrySubDivision")
    private CountrySubDivision countrySubDivision;
    @XmlElement(name = "Date")
    private Date date;
    @XmlElement(name = "ProximityDistanceMeasure")
    private Double proximityDistanceMeasure;
    @XmlElement(name = "Station")
    private Station station;
    @XmlElement(name = "Time")
    private Date time;
}
