package com.ati.ndc.commons.model.enumeration;

public enum JourneyStageCode {
    AFTER_DEPARTURE,
    NO_SHOW,
    PRIOR_TO_DEPARTURE
}
