package com.ati.ndc.commons.model.offerprice;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@XmlRootElement(name = "Fee")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class Fee {
    @XmlElement(name = "Amount")
    private Double amount;  //cant be null
    @XmlElement(name = "ApproximateInd")
    private Boolean approximateInd;
    @XmlElement(name = "DescText")
    private String descText;
    @XmlElement(name = "DesigText")
    private String desigText;
    @XmlElement(name = "GuaranteeTimeLimitDateTime")
    private Date guaranteeTimeLimitDateTime;
    @XmlElement(name = "LocalAmount")
    private Double localAmount;
    @XmlElement(name = "RefundInd")
    private Boolean refundInd;
}
