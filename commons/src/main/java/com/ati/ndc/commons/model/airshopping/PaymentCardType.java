package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "PaymentCardType")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class PaymentCardType {
    @XmlElement(name = "CardBrandCode")
    private String cardBrandCode;   //cant be null
    @XmlElement(name = "CardIssuingCountryCode")
    private String cardIssuingCountryCode;  //token with pattern value [A-Z]{2}
    @XmlElement(name = "CardProductTypeCode")
    private String cardProductTypeCode;
    @XmlElement(name = "CardTypeText")
    private String cardTypeText;
    @XmlElement(name = "CryptographyKey")
    private CryptographyKey cryptographyKey;
    @XmlElement(name = "SecurePayerAuthenticationVersion")
    private SecurePayerAuthenticationVersion securePayerAuthenticationVersion;
    @XmlElement(name = "VerificationInd")
    private Boolean verificationInd;
}
