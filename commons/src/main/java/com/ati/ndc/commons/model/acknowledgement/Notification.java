package com.ati.ndc.commons.model.acknowledgement;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Notification")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Notification {
    @XmlElement(name = "StatusCode")
    private String statusCode; //token
    @XmlElement(name = "StatusMessagetext")
    private String statusMessagetext;
}
