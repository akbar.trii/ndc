package com.ati.ndc.commons.model.invguarantee;

import com.ati.ndc.commons.model.enumeration.OwnerTypeCode;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;
import java.util.List;

@XmlRootElement(name = "OfferItemRQ")
@XmlAccessorType(XmlAccessType.FIELD)
public class OfferItemRQ {
    @XmlElement(name = "OfferItemID")
    private String offerItemId;
    @XmlElement(name = "OfferItemTypeCode")
    private String offerItemTypeCode;
    @XmlElement(name = "OwnerCode")
    private String ownerCode;   //token with pattern value ([A-Z]{3}|[A-Z]{2})|([0-9][A-Z])|([A-Z][0-9])
    @XmlElement(name = "OwnerTypeCode")
    private OwnerTypeCode ownerTypeCode;
    @XmlElement(name = "PriceGuaranteeTimeLimitDateTime")
    private Date priceGuaranteeTimeLimitDateTime;
    @XmlElement(name = "relatedtoexistingOrderItem")
    private List<OrderItem> relatedtoexistingOrderItem;
    @XmlElement(name = "WebAddressURL")
    private String webAddressURL;
}