package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "IATA_AirShoppingRS")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class IATAAirShoppingRS {

    // one of these element shud has value (choice)
    @XmlElement(name = "Error")
    private List<Error> error;
    @XmlElement(name = "Response")
    private Response response;
    //end

    @XmlElement(name = "AugmentationPoint")
    private List<String> augmentationPoint;   //value is any element, still wrong datatype and cant be null
    @XmlElement(name = "MessageDoc")
    private MessageDoc messageDoc;
    @XmlElement(name = "PayloadAttributes")
    private PayloadStandardAttributes payloadAttributes;

    //lihat xml lagi karna masih ada tambahan
}