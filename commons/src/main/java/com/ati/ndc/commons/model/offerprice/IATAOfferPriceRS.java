package com.ati.ndc.commons.model.offerprice;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "IATA_OfferPriceRS")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class IATAOfferPriceRS {
    // one of these element shud has value (choice)
    @XmlElement(name = "Error")
    private List<Error> error;
    @XmlElement(name = "Response")
    private ResponseType response;
    //end

    @XmlElement(name = "AugmentationPoint")
    private List<String> augmentationPoint;   //value is any element, still wrong datatype and cant be null
    @XmlElement(name = "MessageDoc")
    private MessageDoc messageDoc;
    @XmlElement(name = "PayloadAttributes")
    private PayloadStandardAttributes payloadAttributes;
    //lihat xml lagi karna masih ada tambahan
}