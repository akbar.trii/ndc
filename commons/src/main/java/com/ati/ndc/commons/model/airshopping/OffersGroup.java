package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "OffersGroup")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class OffersGroup {
    @XmlElement(name = "AllOffersSummary")
    private AllOffersSummary allOffersSummary;
    @XmlElement(name = "CarrierOffers")
    private List<CarrierOffers> carrierOffers;    //cant be null
}
