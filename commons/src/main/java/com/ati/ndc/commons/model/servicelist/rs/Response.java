package com.ati.ndc.commons.model.servicelist.rs;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "Response")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Response {
    @XmlElement(name = "ALaCarteOffer")
    private AlaCarteOffer aLaCarteOffer;
    @XmlElement(name = "DataLists")
    private DataLists dataLists;
    @XmlElement(name = "Metadata")
    private Metadata metadata;
    @XmlElement(name = "PaymentFunctions")
    private PaymentFunctions paymentFunctions;
    @XmlElement(name = "ResponseParameters")
    private ResponseParameters responseParameters;
    @XmlElement(name = "ServiceListProcessing")
    private ServiceListProcessing serviceListProcessing;
    @XmlElement(name = "ShoppingResponse")
    private ShoppingResponse shoppingResponse;
    @XmlElement(name = "Warning")
    private List<Warning> warning;
}
