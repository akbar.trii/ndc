package com.ati.ndc.commons.model;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "TermOfferPriceRS")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class TermOfferPriceRS {
//    @XmlElement(name = "AvailPeriod")
//    private AvailPeriod availPeriod;
//    @XmlElement(name = "Desc")
//    private Desc desc;
//    @XmlElement(name = "OrderingQty")
//    private OrderingQty orderingQty;
    @XmlElement(name = "TermID")
    private String termId;  //cant be null
}
