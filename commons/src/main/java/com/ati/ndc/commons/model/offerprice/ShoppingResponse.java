package com.ati.ndc.commons.model.offerprice;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "ShoppingResponse")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class ShoppingResponse {
    @XmlElement(name = "ShoppingResponseRefID")
    private String shoppingResponseRefId;
}
