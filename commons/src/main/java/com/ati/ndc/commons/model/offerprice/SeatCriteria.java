package com.ati.ndc.commons.model.offerprice;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "SeatCriteria")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class SeatCriteria {
    @XmlElement(name = "ColumnID")
    private String columnId;    //cant be null
    @XmlElement(name = "RowNumber")
    private Integer rowNumber;  //cant be null
    @XmlElement(name = "SeatCharacteristicCode")
    private List<String> seatCharacteristicCode;    //max 99 elements
    @XmlElement(name = "SeatProfileRefID")
    private String seatProfileRefId;    //cant be null
}
