package com.ati.ndc.commons.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Voucher {
    private Date effectiveDate;
    private Date expirationDate;
    private Double remainingAmount;
    private String voucherId;
}
