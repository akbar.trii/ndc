package com.ati.ndc.commons.model.servicelist.rs;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "ShoppingResponseRS")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class ShoppingResponse {
    @XmlElement(name = "ShoppingResponseRefID")
    private String shoppingResponseRefId;
}
