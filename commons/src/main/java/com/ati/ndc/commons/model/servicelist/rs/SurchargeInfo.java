package com.ati.ndc.commons.model.servicelist.rs;

import com.ati.ndc.commons.model.enumeration.RoundingPrecisionCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "SurchargeInfo")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SurchargeInfo {
    @XmlElement(name = "PaymentFeeAmountRangeMaximumAmount")
    private Double paymentFeeAmountRangeMaximumAmount;
    @XmlElement(name = "PaymentFeeAmountRangeMinimumAmount")
    private Double paymentFeeAmountRangeMinimumAmount;
    @XmlElement(name = "PaymentFeePercentageRangeMaximumPercent")
    private Double paymentFeePercentageRangeMaximumPercent;
    @XmlElement(name = "PaymentFeePercentageRangeMinimumPercent")
    private Double paymentFeePercentageRangeMinimumPercent;
    @XmlElement(name = "PaymentFeeRoundingNumberOfDecimalsNumber")
    private Double paymentFeeRoundingNumberOfDecimalsNumber;
    @XmlElement(name = "PaymentFeeRoundingPrecisionCode")
    private RoundingPrecisionCode paymentFeeRoundingPrecisionCode;
}
