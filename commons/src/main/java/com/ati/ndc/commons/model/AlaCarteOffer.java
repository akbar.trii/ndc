package com.ati.ndc.commons.model;

import com.ati.ndc.commons.model.enumeration.MatchTypeCode;
import com.ati.ndc.commons.model.enumeration.OwnerTypeCode;
import lombok.Data;

import java.util.List;

@Data
public class AlaCarteOffer {
    private List<AlaCarteOfferItem> aLaCarteOfferItem;    //cant be null
    private List<String> baggageDisclosureRefId;
//    private List<Desc> desc;
    private String disclosureRefId;
//    private JourneyOverview journeyOverview;
    private String matchAppText;
    private Double matchPercent;
    private MatchTypeCode matchTypeCode;
    private String OfferId; //cant be null
    private String OwnerCode;   //token with value pattern ([A-Z]{3}|[A-Z]{2})|([0-9][A-Z])|([A-Z][0-9]) and cant be null
    private OwnerTypeCode ownerTypeCode;
    private List<String> penaltyRefId;
//    private List<PTCOfferParameters> ptcOfferParameters;
    private Boolean redemptionInd;
    private Boolean requestedDateInd;
    private Double totalPrice;
    private String validatingCarrierCode;   //token with value pattern ([A-Z]{3}|[A-Z]{2})|([0-9][A-Z])|([A-Z][0-9])
    private String webAddressURL;
}