package com.ati.ndc.commons.model.offerprice;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "ServiceRQ")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ServiceRQ {
    @XmlElement(name = "BookingRef")
    private List<BookingRef> bookingRef;    //max 3 elements
    @XmlElement(name = "ServiceID")
    private String serviceId;   //cant be null
    @XmlElement(name = "ValidatingCarrier")
    private Carrier validatingCarrier;
}
