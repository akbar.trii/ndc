package com.ati.ndc.commons.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SecurePaymentAuthenticationInstructionsVersion1 {
    private String authenticationTransactionIdentifier; //token
    private String merchantData;
    private String payerAuthenticationRequestText;
    private String redirectionURL;
    private String terminationURL;
}