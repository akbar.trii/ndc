package com.ati.ndc.commons.model;

import com.ati.ndc.commons.model.enumeration.GenderCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;
import java.util.List;

@XmlRootElement(name = "Pax")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class IdentityDoc {
//    @XmlElement(name = "AddlName")
//    private AddlName addlName;
    @XmlElement(name = "Birthdate")
    private Date birthdate;
    @XmlElement(name = "BirthplaceText")
    private String birthplaceText;
    @XmlElement(name = "CitizenshipCountryCode")
    private String citizenshipCountryCode;  //token with value pattern [A-Z]{2}
    @XmlElement(name = "ExpiryDate")
    private Date expiryDate;
    @XmlElement(name = "GenderCode")
    private GenderCode genderCode;
    @XmlElement(name = "GivenName")
    private String givenName;   //max 5
    @XmlElement(name = "IdentityDocID")
    private String identityDocId;  //cant be null
    @XmlElement(name = "IdentityDocTypeCode")
    private String identityDocTypeCode;    //token and cant be null
    @XmlElement(name = "IssueDate")
    private Date issueDate;
    @XmlElement(name = "IssuingCountryCode")
    private String issuingCountryCode;  //token with value pattern [A-Z]{2}
    @XmlElement(name = "MiddleName")
    private String middleName;
    @XmlElement(name = "ResidenceCountryCode")
    private String residenceCountryCode;    //token with value pattern [A-Z]{2}
    @XmlElement(name = "SuffixName")
    private String suffixName;
    @XmlElement(name = "Surname")
    private String surname;
    @XmlElement(name = "TitleName")
    private String titleName;
//    @XmlElement(name = "Visa")
//    private List<Visa> visa;
}