package com.ati.ndc.commons.model;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@XmlRootElement(name = "EnabledSystemAirlineProfileNotifRQ")
@XmlAccessorType(XmlAccessType.FIELD)
public class EnabledSystemAirlineProfileNotifRQ {
    @XmlElement(name = "ContactInfo")
    private List<ContactInfoAirlineProfileNotifRQ> contactInfo;
    @XmlElement(name = "Name")
    private String name;
    @XmlElement(name = "SystemID")
    private String systemId;   //cant be null
}
