package com.ati.ndc.commons.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CashType {
    private Double netClearanceAmount; //cant be null
    private Date remittanceDate;    //cant be null
}
