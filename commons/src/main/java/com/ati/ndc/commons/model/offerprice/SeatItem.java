package com.ati.ndc.commons.model.offerprice;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "SeatItem")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SeatItem {
    @XmlElement(name = "ColumnID")
    private String columnId;
    @XmlElement(name = "DatedOperatingLegRefID")
    private String datedOperatingLegRefId;
    @XmlElement(name = "PaxSegmentRefID")
    private String paxSegmentRefId; //cant be null
    @XmlElement(name = "Price")
    private Price price;
    @XmlElement(name = "RowNumber")
    private Integer rowNumber;
    @XmlElement(name = "SeatProfileRefID")
    private List<String> seatProfileRefId;
}