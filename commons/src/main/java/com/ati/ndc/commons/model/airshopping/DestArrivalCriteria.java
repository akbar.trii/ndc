package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@Data
@XmlRootElement(name = "DestArrivalCriteria")
@XmlAccessorType(XmlAccessType.FIELD)
public class DestArrivalCriteria {
    @XmlElement(name = "BoardingGateID")
    private String boardingGateId;
    @XmlElement(name = "Date")
    private Date date;
    @XmlElement(name = "IATA_LocationCode")
    private String iataLocationCode;    //token with length 3 and cant be null
    @XmlElement(name = "StationName")
    private String stationName;
    @XmlElement(name = "TerminalName")
    private String terminalName;
    @XmlElement(name = "Time")
    private Date time;
    @XmlElement(name = "TimeAfterMeasure")
    private Double TimeAfterMeasure;
    @XmlElement(name = "TimeBeforeMeasure")
    private Double TimeBeforeMeasure;
}
