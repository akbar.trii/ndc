package com.ati.ndc.commons.model.airshopping;

import com.ati.ndc.commons.model.enumeration.ObjectNameCodeType;
import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "PayloadRestrictions")
@XmlAccessorType(XmlAccessType.FIELD)
public class PayloadRestrictions {
    @XmlElement(name = "Limit")
    private Double limit;
    @XmlElement(name = "ObjectName")
    private ObjectNameCodeType objectName;
}
