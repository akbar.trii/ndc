package com.ati.ndc.commons.model.servicelist.rs;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;
import java.util.List;

@XmlRootElement(name = "Price")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Price {
    @XmlElement(name = "BaseAmount")
    private Double baseAmount;
    @XmlElement(name = "BaseAmountGuaranteeTimeLimitDateTime")
    private Date baseAmountGuaranteeTimeLimitDateTime;
    @XmlElement(name = "CurConversion")
    private List<CurConversion> curConversion;
    @XmlElement(name = "Discount")
    private Discount discount;
    @XmlElement(name = "EquivAmount")
    private Double equivAmount;
    @XmlElement(name = "Fee")
    private List<Fee> fee;
    @XmlElement(name = "LoyaltyUnitAmount")
    private Double loyaltyUnitAmount;
    @XmlElement(name = "LoyaltyUnitName")
    private String loyaltyUnitName;
    @XmlElement(name = "Markup")
    private List<Markup> markup;
    @XmlElement(name = "MaskedInd")
    private Boolean maskedInd;
    @XmlElement(name = "Surcharge")
    private List<Surcharge> surcharge;
    @XmlElement(name = "TaxSummary")
    private List<TaxExemption> taxSummary;
    @XmlElement(name = "TotalAmount")
    private Double totalAmount;
}