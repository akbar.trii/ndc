package com.ati.ndc.commons.model.paymentclearancenotif;

import com.ati.ndc.commons.model.ClearanceType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IATA_PaymentClearanceNotif {
    private List<ClearanceType> clearance;  //cant be null
    private Integer clearanceCount; //cant be null
//    private PayloadStandardAttributes payloadStandardAttributes;
}