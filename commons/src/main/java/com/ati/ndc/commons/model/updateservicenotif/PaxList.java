package com.ati.ndc.commons.model.updateservicenotif;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "PaxList")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class PaxList {
    @XmlElement(name = "Pax")
    private List<Pax> pax;    //cant be null
}