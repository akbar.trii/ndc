package com.ati.ndc.commons.model.airdocnotif;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "EnabledSystem")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class EnabledSystem {
    @XmlElement(name = "ContactInfo")
    private List<ContactInfo> contactInfo;
    @XmlElement(name = "ContactInfoRefID")
    private List<String> contactInfoRefId;
    @XmlElement(name = "Name")
    private String name;
    @XmlElement(name = "SystemID")
    private String systemId;   //cant be null
}