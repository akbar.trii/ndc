package com.ati.ndc.commons.model.servicelist.rq;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@XmlRootElement(name = "IATA_EasyPayCriteria")
@XmlAccessorType(XmlAccessType.FIELD)
public class OrderAssociation {
    @XmlElement(name = "OrderItemRefID")
    private List<String> orderItemRefId;
    @XmlElement(name = "OrderRefID")
    private String orderRefId;
}