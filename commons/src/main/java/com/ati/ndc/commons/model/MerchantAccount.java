package com.ati.ndc.commons.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MerchantAccount {
//    private Carrier carrier;
//    private Country country;
    private String merchantCategoryCode;
    private String merchantId;
    private String merchantName;
    private String merchantRiskText;
}