package com.ati.ndc.commons.model.enumeration;

public enum ServiceDeliveryStatusCodeType {
    DELIVERED,
    EXPIRED,
    FAILED_TO_DELIVER,
    IN_PROGRESS,
    NOT_CLAIMED,
    READY_TO_DELIVER,
    READY_TO_PROCEED,
    REMOVED,
    SUSPENDED,
    UNABLE_TO_DELIVER
}
