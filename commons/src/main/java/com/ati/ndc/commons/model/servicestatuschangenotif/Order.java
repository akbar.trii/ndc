package com.ati.ndc.commons.model.servicestatuschangenotif;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "Order")
@XmlAccessorType(XmlAccessType.FIELD)
public class Order {
    @XmlElement(name = "OrderID")
    private String orderId; //cant be null
    @XmlElement(name = "OrderItem")
    private List<OrderItem> orderItem;  //cant be null
    @XmlElement(name = "OrderVersionNumber")
    private Integer orderVersionNumber;
}