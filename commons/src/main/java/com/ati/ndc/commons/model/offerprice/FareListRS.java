package com.ati.ndc.commons.model.offerprice;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "FareListRS")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class FareListRS {
    @XmlElement(name = "FareGroup")
    private List<FareGroupRS> fareGroup;    //cant be null
}
