package com.ati.ndc.commons.model.servicelist.rq;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;
import java.util.List;

@XmlRootElement(name = "OfferItem")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class OfferItem {
    @XmlElement(name = "Desc")
    private List<Desc> desc;
    @XmlElement(name = "OfferItemID")
    private String offerItemId;
    @XmlElement(name = "OfferItemTypeCode")
    private String offerItemTypeCode;
    @XmlElement(name = "OwnerCode")
    private String OwnerCode;   //token with pattern value ([A-Z]{3}|[A-Z]{2})|([0-9][A-Z])|([A-Z][0-9])
    @XmlElement(name = "PriceGuaranteeTimeLimitDateTime")
    private Date priceGuaranteeTimeLimitDateTime;
    @XmlElement(name = "relatedtoexistingOrderItem")
    private List<OrderItem> relatedtoexistingOrderItem;
    @XmlElement(name = "Service")
    private List<Service> service;
    @XmlElement(name = "ServiceTaxonomy")
    private List<ServiceTaxonomy> serviceTaxonomy;
}
