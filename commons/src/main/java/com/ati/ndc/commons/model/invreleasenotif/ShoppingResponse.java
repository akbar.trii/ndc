package com.ati.ndc.commons.model.invreleasenotif;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "ShoppingResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class ShoppingResponse {
    @XmlElement(name = "Carrier")
    private Carrier carrier;
    @XmlElement(name = "OwnerCode")
    private String ownerCode;   //token with value pattern ([A-Z]{3}|[A-Z]{2})|([0-9][A-Z])|([A-Z][0-9]) and cant be null
    @XmlElement(name = "ShoppingResponseRefID")
    private String shoppingResponseRefId;
}