package com.ati.ndc.commons.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PaymentClearanceCancellationRS {
    // one of these element shud has value (choice)
    private List<Clearance> clearance;
    private List<Error> error;
    //end

//    private PayloadStandardAttributes payloadStandardAttributes;
}
