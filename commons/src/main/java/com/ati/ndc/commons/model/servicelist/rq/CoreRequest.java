package com.ati.ndc.commons.model.servicelist.rq;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@XmlRootElement(name = "ShoppingCriteria")
@XmlAccessorType(XmlAccessType.FIELD)
public class CoreRequest {
    //choices
    @XmlElement(name = "Offer")
    private List<Offer> offer;
    @XmlElement(name = "Order")
    private Order order;
    @XmlElement(name = "OriginDest")
    private List<OriginDest> originDest;
}
