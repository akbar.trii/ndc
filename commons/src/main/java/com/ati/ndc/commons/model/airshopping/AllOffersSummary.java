package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "AllOffersSummary")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class AllOffersSummary {
    @XmlElement(name = "HighestOfferPrice")
    private PriceSummaryType highestOfferPrice;
    @XmlElement(name = "LowestOfferPrice")
    private PriceSummaryType lowestOfferPrice;
    @XmlElement(name = "MatchedOfferQty")
    private Double matchedOfferQty;
}
