package com.ati.ndc.commons.model.offerprice;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "BagItemDetails")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BagItemDetails {
    @XmlElement(name = "BaggageAllowanceRefID")
    private String baggageAllowanceRefId;   //cant be null
    @XmlElement(name = "BaggageDisclosureRefID")
    private String baggageDisclosureRefId;
    @XmlElement(name = "Price")
    private Price price;
    @XmlElement(name = "ValidatingCarrierCode")
    private String validatingCarrierCode;   //token with pattern value ([A-Z]{3}|[A-Z]{2})|([0-9][A-Z])|([A-Z][0-9])
}
