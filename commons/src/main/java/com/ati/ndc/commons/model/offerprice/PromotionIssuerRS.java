package com.ati.ndc.commons.model.offerprice;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "PromotionIssuerRS")
@XmlAccessorType(XmlAccessType.FIELD)
public class PromotionIssuerRS {
    @XmlElement(name = "Carrier")
    private Carrier carrier;    //cant be null
    @XmlElement(name = "Org")
    private Org org;    //cant be null
    @XmlElement(name = "TravelAgency")
    private TravelAgencyRS travelAgency;   //cant be null
}
