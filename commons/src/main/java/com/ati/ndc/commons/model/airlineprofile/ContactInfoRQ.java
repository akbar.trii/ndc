package com.ati.ndc.commons.model.airlineprofile;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "ContactInfoRQ")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ContactInfoRQ {
    @XmlElement(name = "ContactInfoID")
    private String contactInfoId;
    @XmlElement(name = "ContactPurposeText")
    private List<String> contactPurposeText;    //max 99 element
    @XmlElement(name = "ContactRefusedInd")
    private Boolean contactRefusedInd;
    @XmlElement(name = "IndividualRefID")
    private String individualRefId;
    @XmlElement(name = "PaxSegmentRefID")
    private String paxSegmentRefId;
    @XmlElement(name = "RelationshipToPax")
    private String relationshipToPax;
}
