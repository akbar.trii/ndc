package com.ati.ndc.commons.model.updateservicenotif;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "OperatingCarrierInfo")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class OperatingCarrierInfo {
    @XmlElement(name = "CarrierDesigCode")
    private String carrierDesigCode;    //token with pattern value ([A-Z]{3}|[A-Z]{2})|([0-9][A-Z])|([A-Z][0-9])
    @XmlElement(name = "CarrierName")
    private String carrierName; //token with patter value [A-Z]{2}
    @XmlElement(name = "DatedOperatingLeg")
    private List<DatedOperatingLeg> datedOperatingLeg;    //cant be null
    @XmlElement(name = "Disclosure")
    private Disclosure disclosure;
    @XmlElement(name = "OperatingCarrierFlightNumberText")
    private String operatingCarrierFlightNumberText;
    @XmlElement(name = "OperationalSuffixText")
    private String operationalSuffixText;
    @XmlElement(name = "RBD_Code")
    private String rbdCode;
    @XmlElement(name = "StatusCode")
    private String statusCode;
}