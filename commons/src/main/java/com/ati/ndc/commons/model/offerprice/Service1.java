package com.ati.ndc.commons.model.offerprice;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "Service1")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class Service1 {
    @XmlElement(name = "InterlineSettlementInfo")
    private List<InterlineSettlementInfoRQ> interlineSettlementInfo;
    @XmlElement(name = "PaxRefID")
    private List<String> paxRefId;  //cant be null
    @XmlElement(name = "ServiceAssociations")
    private ServiceAssociations serviceAssociations;   //cant be null
    @XmlElement(name = "ServiceID")
    private String serviceId;   //cant be null
    @XmlElement(name = "ServiceRefID")
    private String serviceRefId;
    @XmlElement(name = "ServiceTaxonomy")
    private List<ServiceTaxonomy> serviceTaxonomy;
    @XmlElement(name = "ValidatingCarrier")
    private Carrier validatingCarrier;
}