package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "CalendarDateCriteria")
@XmlAccessorType(XmlAccessType.FIELD)
public class CalendarDateCriteria {
    @XmlElement(name = "DaysAfterNumber")
    private Double daysAfterNumber;
    @XmlElement(name = "DaysBeforeNumber")
    private Double daysBeforeNumber;
}
