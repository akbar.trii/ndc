package com.ati.ndc.commons.model.orderview;

import com.ati.ndc.commons.model.PaymentFunctions;
import com.ati.ndc.commons.model.orderhistory.rs.IATA_OrderHistoryRS;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IATA_OrderViewRS extends IATA_OrderHistoryRS {
    List<PaymentFunctions> paymentFunctions;
}