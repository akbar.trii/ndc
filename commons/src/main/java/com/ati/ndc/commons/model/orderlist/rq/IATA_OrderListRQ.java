package com.ati.ndc.commons.model.orderlist.rq;

import com.ati.ndc.commons.model.orderchange.IATA_OrderChangeRQ;
import lombok.Data;

@Data
public class IATA_OrderListRQ extends IATA_OrderChangeRQ {
}