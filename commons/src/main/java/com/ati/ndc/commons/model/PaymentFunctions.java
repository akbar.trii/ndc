package com.ati.ndc.commons.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PaymentFunctions {
    private List<PaymentProcessingSummary> paymentProcessingSummary;
    private List<PaymentSupportedMethod> paymentSupportedMethod;
}