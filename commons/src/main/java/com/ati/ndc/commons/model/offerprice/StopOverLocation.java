package com.ati.ndc.commons.model.offerprice;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "StopOverLocation")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class StopOverLocation {
    @XmlElement(name = "DatedOperatingLegRefID")
    private String datedOperatingLegRefId;
    @XmlElement(name = "MaximumDuration")
    private String maximumDuration;
    @XmlElement(name = "PaxSegmentRefID")
    private String paxSegmentRefId;
}
