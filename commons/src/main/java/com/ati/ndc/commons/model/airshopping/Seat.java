package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Seat")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class Seat {
    @XmlElement(name = "ColumnID")
    private String columnId;    //cant be null
    @XmlElement(name = "RowNumber")
    private Integer rowNumber;  //cant be null
}