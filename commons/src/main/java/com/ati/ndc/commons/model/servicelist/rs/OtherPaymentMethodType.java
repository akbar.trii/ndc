package com.ati.ndc.commons.model.servicelist.rs;

import com.ati.ndc.commons.model.offerprice.Remark;
import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "OtherPaymentMethodType")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class OtherPaymentMethodType {
    @XmlElement(name = "Remark")
    private List<Remark> remark;
}
