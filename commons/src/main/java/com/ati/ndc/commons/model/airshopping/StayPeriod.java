package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;
import java.util.List;

@Data
@XmlRootElement(name = "StayPeriod")
@XmlAccessorType(XmlAccessType.FIELD)
public class StayPeriod {
    @XmlElement(name = "EndDate")
    private Date endDate;
    @XmlElement(name = "MaximumStayNumber")
    private Double maximumStayNumber;
    @XmlElement(name = "MinimumStayNumber")
    private Double minimumStayNumber;
    @XmlElement(name = "StartDate")
    private Date startDate;
    @XmlElement(name = "StayMonthName")
    private List<String> stayMonthName;   //max 12 element
    @XmlElement(name = "StayQuarterText")
    private List<String> stayQuarterText; //max 4 element
}
