package com.ati.ndc.commons.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PaymentAddressVerification {
    private Boolean addressVerificationInvalidInd;
    private Boolean addressVerificationNoMatchInd;
    private String addressVerificationStatusCode;    //token
    private String addressVerificationText;
}
