package com.ati.ndc.commons.model;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class OfferItem {
//    private List<CancelRestrictions> cancelRestrictions;
//    private List<ChangeRestrictions> changeRestrictions;
//    private List<Commission> commission;
//    private List<DescType> desc;
//    private List<FareDetail> fareDetail;
    private Boolean mandatoryInd;
    private Boolean modificationProhibitedInd;
    private String offerItemId; //cant be null
    private String offerItemTypeCode;
//    private PaymentTimeLimit paymentTimeLimit;
    private PriceType price;    //cant be null
    private Date priceGuaranteeTimeLimitDateTime;
    private List<String> service;   //cant be null
//    private List<ServiceTaxonomy> serviceTaxonomy;
//    private StopOverRestrictions stopOverRestrictions;
}
