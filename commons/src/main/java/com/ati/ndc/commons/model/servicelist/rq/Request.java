package com.ati.ndc.commons.model.servicelist.rq;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "Request")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Request {
    @XmlElement(name = "CoreRequest")
    private CoreRequest coreRequest;    //cant be null
    @XmlElement(name = "Metadata")
    private Metadata metadata;
    @XmlElement(name = "OriginDest")
    private OriginDest originDest;
    @XmlElement(name = "Pax")
    private List<Pax> pax;
    @XmlElement(name = "Policy")
    private List<Policy> policy;
    @XmlElement(name = "ResponseParameters")
    private ResponseParameters responseParameters;
    @XmlElement(name = "ShoppingCriteria")
    private ShoppingCriteria shoppingCriteria;
    @XmlElement(name = "ShoppingResponse")
    private ShoppingResponse shoppingResponse;
}
