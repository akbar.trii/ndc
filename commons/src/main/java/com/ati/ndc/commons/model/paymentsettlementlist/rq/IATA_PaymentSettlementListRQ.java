package com.ati.ndc.commons.model.paymentsettlementlist.rq;

import com.ati.ndc.commons.model.RemittanceDate;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IATA_PaymentSettlementListRQ {
    //choices, must pick one of these var
    private List<String> settleId;  //token length 10 with pattern (SWS|BIL)[A-Za-z0-9]{7}
    private List<RemittanceDate> settlementDate;
    //end
//    private PayloadStandardAttributes payloadStandardAttributes;
}