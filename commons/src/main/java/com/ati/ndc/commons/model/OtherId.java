package com.ati.ndc.commons.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OtherId {
    private List<String> agreementId;   //token maxlength 35
    private List<String> commitmentId;  //token maxlength 35
}
