package com.ati.ndc.commons.model.enumeration;

public enum RoundingPrecisionCode {
    Down,   //round down
    Up      //round up
}
