package com.ati.ndc.commons.model.invreleasenotif;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Associations")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Associations {
    @XmlElement(name = "Offer")
    private Offer offer;
    @XmlElement(name = "Order")
    private Order order;
    @XmlElement(name = "ShoppingResponse")
    private ShoppingResponse shoppingResponse;
}
