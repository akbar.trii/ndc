package com.ati.ndc.commons.model.offerprice;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "Request")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Request {
    @XmlElement(name = "DataLists")
    private DataListsRQ dataLists;
    @XmlElement(name = "InExchForTicket")
    private InExchForTicket inExchForTicket;
    @XmlElement(name = "Metadata")
    private Metadata metadata;
    @XmlElement(name = "OfferPriceParameters")
    private ResponseParameters offerPriceParameters;
    @XmlElement(name = "OriginDestRefID")
    private List<String> originDestRefId;
    @XmlElement(name = "Policy")
    private List<Policy> policy;
    @XmlElement(name = "PricedOffer")
    private PricedOffer pricedOffer;    //cant be null
    @XmlElement(name = "ShoppingCriteria")
    private ShoppingCriteria shoppingCriteria;
}
