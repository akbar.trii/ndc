package com.ati.ndc.commons.model.invguarantee;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "InventoryGuaranteeProcessing")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class InventoryGuaranteeProcessing {
    @XmlElement(name = "CurParameter")
    private List<CurParameter> curParameter;
    @XmlElement(name = "Policy")
    private List<Policy> policy;
    @XmlElement(name = "Remark")
    private List<Remark> remark;
}
