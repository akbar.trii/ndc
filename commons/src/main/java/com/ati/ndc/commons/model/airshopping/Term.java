package com.ati.ndc.commons.model.airshopping;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Term")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class Term {
    @XmlElement(name = "AvailPeriod")
    private AvailPeriod availPeriod;    //cant be null
    @XmlElement(name = "Desc")
    private Desc desc;  //cant be null
    @XmlElement(name = "OrderingQty")
    private OrderingQty orderingQty;    //cant be null
    @XmlElement(name = "TermID")
    private String termId;  //cant be null
}
